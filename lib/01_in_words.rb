class Fixnum

  def three_dig_words(num)
    u20 = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight',
           'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen',
           'sixteen', 'seventeen', 'eighteen', 'nineteen']
    tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy',
            'eighty', 'ninety']

    words = []
    if num % 100 < 20
      words.unshift u20[num % 100]
    else
      words.unshift u20[num % 10]
      words.unshift tens[num % 100 / 10]
    end

    if num >= 100
      words.unshift 'hundred'
      words.unshift u20[num / 100]
    end

    words.join(' ').strip
  end

  def in_words
    return 'zero' if zero?

    powers = ['', 'thousand', 'million', 'billion', 'trillion']
    words = ''
    (0..to_s.length).step(3) do |i|
      three_dig = (self % 10**(i + 3)) / (10**i)
      unless three_dig.zero?
        words.prepend("#{three_dig_words(three_dig)} #{powers[i / 3]} ")
      end
    end
    words.strip
  end
end
